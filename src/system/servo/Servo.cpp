/*
 * Servo.cpp
 *
 *  Created on: 07 ���. 2014 �.
 *      Author: Alexey
 */

#include "Servo.h"

Servo::Servo() {

	TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11);
	TCCR1B |= (1 << WGM13) | (1 << WGM12) | (1 << CS11) | (1 << CS10);

	ICR1 = PERIOD_VALUE;
	OCR1A = MIDDLE_WHEEL_VALUE;	// wheel
	OCR1B = MIDDLE_ENGINE_VALUE;	// engine
	DDRD |= 0x30;
}

Servo::~Servo() {
	// TODO Auto-generated destructor stub
}

void Servo::setWheelAngle(uint8_t angle) {
	// OCR1A = 375 + angle - 90;
	// TODO need to realize
}

/**
 * ������������� ��������� ������� �������.
 * ����������� ��������.
 * 0 - ��������� �����
 * -100..0 - ��������� �����
 * 0..100 - ��������� ������
 * @param value - �������� � ��������� �� -100 �� 100
 */
void Servo::setWheelValue(signed char value) {
	OCR1A = MIDDLE_WHEEL_VALUE + value;
}

void Servo::setSpeed(signed char speed) {
	if (speed < 0) {
		speed = 0;
	} else if (speed > MAX_UP_SPEED) {
		speed = MAX_UP_SPEED;
	}
	OCR1B = MIDDLE_ENGINE_VALUE + speed;
}

void Servo::stopMove(uint8_t stop_speed) {
	if (stop_speed < 0) {
		stop_speed = 0;
	} else if (stop_speed > MAX_STOP) {
		stop_speed = MAX_STOP;
	}
	OCR1B = MIDDLE_ENGINE_VALUE - stop_speed;
}

void Servo::stopMove() {
	OCR1B = MIDDLE_ENGINE_VALUE - MAX_STOP;
}

void Servo::handle(uint8_t value) {

}
