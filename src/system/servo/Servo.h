/*
 * Servo.h
 *
 *  Created on: 07 ���. 2014 �.
 *      Author: Alexey
 */

#ifndef SERVO_H_
#define SERVO_H_

#include <avr/io.h>

#define PERIOD_VALUE 5000
#define MIDDLE_WHEEL_VALUE 380
#define MIDDLE_ENGINE_VALUE 385
#define MAX_UP_SPEED 100
#define MAX_STOP 90

class Servo{
public:
	Servo();
	virtual ~Servo();
	void setWheelAngle(uint8_t angle);
	void setWheelValue(signed char value);
	void setSpeed(signed char speed);
	void stopMove(uint8_t stop_speed);
	void stopMove();
	virtual void handle(uint8_t value);
};

#endif /* SERVO_H_ */
