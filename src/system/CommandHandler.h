/*
 * CommandHandler.h
 *
 *  Created on: 08 ���. 2014 �.
 *      Author: Alexey
 */

#ifndef COMMANDHANDLER_H_
#define COMMANDHANDLER_H_
#include <avr/io.h>
#include "Command.h"
class CommandHandler {
public:
	CommandHandler();
	virtual ~CommandHandler();
	virtual void handle(Command* command) = 0;
};

#endif /* COMMANDHANDLER_H_ */
