/*
 * System.h
 *
 *  Created on: 09 ���. 2014 �.
 *      Author: Alexey
 */

#ifndef SYSTEM_SYSTEM_H_
#define SYSTEM_SYSTEM_H_
#include "Command.h"
#include "CommandHandler.h"
#include "servo/Servo.h"
#include "uart/UartModule.h"
#include <avr/io.h>
#include <avr/delay.h>
namespace mersedes {

class System : public CommandHandler {
private:
	static System* instance;
	System();
	bool isWorking;

	Command* command;
	Servo* servo;
	UartModule* uart;

public:
	static System* getInstance();
	virtual ~System() {};
	void work();
	void handleUartRXC();
	void handleTimer0_OVF();
	virtual void handle(Command* command);
};

} /* namespace mersedes */

#endif /* SYSTEM_SYSTEM_H_ */
