/*
 * System.cpp
 *
 *  Created on: 09 ���. 2014 �.
 *      Author: Alexey
 */

#include "System.h"

namespace mersedes {
// �� ���� ����� �� ����� ����� ��� �������
System* System::instance;
/**
 * ���������� �������� �������.
 * ���� ������� ��� �� ���� �������������������,
 * �� ���������� �����������, � ������� ���������� ������������� ���� ���������.
 * @return ������ �� ������������ �������� ������ System
 */
System* System::getInstance() {
	if (instance == 0) {
		System system;
		instance = &system;
	}
	return instance;
}

System::System() :
		isWorking(false) {
	servo = new Servo();
	uart = new UartModule();
	command = new Command();
	command->code = 0xAA;
	//uart->setCommandHandler(System::getInstance());
	DDRA = 0xFF;
	sei();
}

void System::work() {
	if (!isWorking) {
		servo->setSpeed(0);
		while (1) {

		}
//		while (1) {
//			//uart->processCommand();
//			PORTA = command->value;
//			if (command->code != 0) {
//
//				switch (command->code) {
//					case COMMAND_SERVO_WHEEL:
//
//						servo->setWheelValue(command->value);
//						break;
//					case COMMAND_SERVO_SPEED:
//						servo->setSpeed(command->value);
//						break;
//					}
//				//command->code = 0;
//			}
//
//
//
//		}
		signed char data = 0;
		signed char modifier = 1;
		while (1) {
			data = data + modifier;
			_delay_ms(2);
			if (data > 100) {
				modifier = -1;
			} else if (data < -100) {
				modifier = 1;
			}
			servo->setWheelValue(data);
			//set_speed(data);
		}
	}
}

void System::handleUartRXC() {
	//uart->notifyRXCEvent();

	char code = UDR;
	while((UCSRA &(1<<RXC)) == 0);
	signed char value  = UDR;
	PORTA = value;
	switch (code) {
	case COMMAND_SERVO_WHEEL:

		servo->setWheelValue(value);
		break;
	case COMMAND_SERVO_SPEED:
		servo->setSpeed(value);
		break;
	}

}

void System::handleTimer0_OVF() {

}

void System::handle(Command* command) {


}

} /* namespace mersedes */
