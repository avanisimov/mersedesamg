/*
 * UartModule.cpp
 *
 *  Created on: 12 ���. 2014 �.
 *      Author: Alexey
 */

#include "UartModule.h"

namespace mersedes {

UartModule::UartModule() :
		handler(0), servo(0) {
	// Set baud rate
	UBRRL = BAUD_PRESCALE; // Load lower 8-bits into the low byte of the UBRR register
	UBRRH = (BAUD_PRESCALE >> 8);
	/* Load upper 8-bits into the high byte of the UBRR register
	 Default frame format is 8 data bits, no parity, 1 stop bit
	 to change use UCSRC, see AVR datasheet*/
	DDRA = 0xFF;
	// Enable receiver and transmitter and receive complete interrupt
	UCSRB = ((1 << TXEN) | (1 << RXEN) | (1 << RXCIE));
	command.code = 0;

}

UartModule::UartModule(CommandHandler* cHandler) :
		servo(0) {
	// Set baud rate
	UBRRL = BAUD_PRESCALE; // Load lower 8-bits into the low byte of the UBRR register
	UBRRH = (BAUD_PRESCALE >> 8);
	/* Load upper 8-bits into the high byte of the UBRR register
	 Default frame format is 8 data bits, no parity, 1 stop bit
	 to change use UCSRC, see AVR datasheet*/
	DDRA = 0xFF;
	// Enable receiver and transmitter and receive complete interrupt
	UCSRB = ((1 << TXEN) | (1 << RXEN) | (1 << RXCIE));
	handler = cHandler;

}

UartModule::~UartModule() {
	//free(command);
}

void UartModule::processCommand() {
	command.code = 0x35;
	command.value = 20;
	handler->handle(&command);

//	if (command.code != 0) {
//		switch (command.code) {
//		case COMMAND_SERVO_WHEEL:
//			servo->setWheelValue(command.value);
//			break;
//		case COMMAND_SERVO_SPEED:
//			servo->setSpeed(command.value);
//			break;
//		}
////		if (handler != 0) {
////			servo->setWheelValue(command.value);
////
////		}
//		command.code = 0;
//	}
}

void UartModule::setCommandHandler(CommandHandler* cHandler) {
	handler = cHandler;
}

void UartModule::sendCommand(Command* command) {

}

void UartModule::notifyRXCEvent() {
	PORTA = 0x08;
	signed char firstByte = UDR;
	command.code = 0x35;
	command.value = firstByte;
//	while((UCSRA &(1<<RXC)) == 0);
//	signed char secondByte = UDR;
//	command.value = secondByte;
//	servo->setWheelValue(firstByte);
//	command = new Command();
//	command->code = 0x35;
//	command->value = firstByte;
//	Command comm;
//	comm.code = 0x35;
//	comm.value = firstByte;
//	handler->handle(&comm);

}

} /* namespace mersedes */
