/*
 * UartModule.h
 *
 *  Created on: 12 ���. 2014 �.
 *      Author: Alexey
 */

#ifndef SYSTEM_UART_UARTMODULE_H_
#define SYSTEM_UART_UARTMODULE_H_
#include <avr/interrupt.h>
#include <avr/io.h>
#include "../Command.h"
#include "../CommandHandler.h"
#include "../servo/Servo.h"
#include "../../cplusplus.h"
#define BAUD_PRESCALE 3

namespace mersedes {

class UartModule {
private:
	Command command;
	CommandHandler* handler;
	Servo* servo;
public:
	UartModule();
	UartModule(CommandHandler* handler);
	virtual ~UartModule();
	void processCommand();
	void setCommandHandler(CommandHandler* handler);
	void sendCommand(Command* command);
	void notifyRXCEvent();
};

} /* namespace mersedes */

#endif /* SYSTEM_UART_UARTMODULE_H_ */
