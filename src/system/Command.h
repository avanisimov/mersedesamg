/*
 * Command.h
 *
 *  Created on: 12 ���. 2014 �.
 *      Author: Alexey
 */

#ifndef SYSTEM_COMMAND_H_
#define SYSTEM_COMMAND_H_

#define COMMAND_SERVO_WHEEL 0x35
#define COMMAND_SERVO_SPEED 0x36

class Command {
public:
	char code;
	signed char value;
};

#endif /* SYSTEM_COMMAND_H_ */
