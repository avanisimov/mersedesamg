/*
 * main.c
 *
 *  Created on: 07 ���. 2014 �.
 *      Author: Alexey
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#include "cplusplus.h"
#include "system/System.h"

using namespace mersedes;

System* system;

int main(void) {
	system = System::getInstance();
	system->work();
}

ISR(USART_RXC_vect) {
	system->handleUartRXC();
}
