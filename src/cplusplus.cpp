/*
 * cplusplus.cpp
 *
 *  Created on: 30 ���. 2014 �.
 *      Author: Alexey
 */

#include "cplusplus.h"

int __cxa_guard_acquire(__guard *g) {
	return !*(char *) (g);
}
;
void __cxa_guard_release(__guard *g) {
	*(char *) g = 1;
}
;
void __cxa_guard_abort(__guard *) {
}
;

/*
 This is applicable if using pure virtual inheritance.
 */


void __cxa_pure_virtual(void) {
}
;

/*
 Operators required for C++
 */


void * operator new(size_t size) {
	return malloc(size);
}

void operator delete(void* ptr) {
	free(ptr);
}


